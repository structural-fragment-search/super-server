{-# LANGUAGE DataKinds #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TypeOperators #-}

module Main where

import Control.Monad.Except (MonadError (throwError))
import Data.Aeson (ToJSON)
import Data.ByteString (ByteString)
import Data.ByteString.Char8 (pack)
import Data.Maybe (fromMaybe, maybe)
import Data.String.Conv (StringConv, toS)
import qualified Data.Text as T
import GHC.Generics (Generic)
import Network.HTTP.Client (Manager)
import Network.HTTP.Client.TLS (newTlsManager)
import Network.Wai (Application)
import Network.Wai.Handler.Warp (run)
import Servant
  ( Application,
    EmptyAPI,
    Handler,
    Proxy (..),
    Server,
    emptyServer,
    err302,
    errHeaders,
    serve,
  )
import Servant.API
  ( Get,
    JSON,
    NoContent (..),
    PostNoContent,
    (:<|>) (..),
    type (:>),
  )
import System.Environment (lookupEnv)
import System.Random
  ( RandomGen,
    StdGen,
    genByteString,
    mkStdGen,
  )
import qualified Web.OIDC.Client as O

data OIDCEnv = OIDCEnv
  { oidc :: O.OIDC,
    rand :: StdGen,
    prov :: O.Provider,
    redirectUri :: ByteString,
    clientId :: ByteString,
    clientSecret :: ByteString
  }

initOidc :: IO OIDCEnv
initOidc = do
  clientId_ <- lookupEnv "CLIENT_ID"
  clientSecret_ <- lookupEnv "CLIENT_SECRET"
  redirectUri_ <- lookupEnv "CLIENT_REDIRECT"
  issLoc <- lookupEnv "ISSUER"
  http <- newTlsManager
  prov <- O.discover (maybe "http://localhost:8080/auth/realms/Super/" T.pack issLoc) http
  let clientId' = maybe "super" pack clientId_
  let clientSecret' = maybe "4bffee46-038b-4582-9cd2-620fad5a8a76" pack clientSecret_
  let redirectUri' = maybe "http://localhost:8081/api/v1/auth/token" pack redirectUri_

  let oidc = O.setCredentials clientId' clientSecret' redirectUri' (O.newOIDC prov)
  let rand = mkStdGen 127
  pure
    OIDCEnv
      { oidc = oidc,
        rand = rand,
        prov = prov,
        redirectUri = redirectUri',
        clientId = clientId',
        clientSecret = clientSecret'
      }

type AuthApi =
  "auth"
    :> ( "login" :> Get '[JSON] NoContent
           :<|> "token" :> PostNoContent
       )

type UsersApi = EmptyAPI

type JobsApi = EmptyAPI

type SuperApi = "api" :> "v1" :> (AuthApi :<|> UsersApi :<|> JobsApi)

redirects :: (StringConv s ByteString) => s -> Handler ()
redirects url = throwError err302 {errHeaders = [("Location", toS url)]}

genOIDCUrl :: OIDCEnv -> Handler ByteString
genOIDCUrl env = do
  let state = fst . genByteString 10 $ rand env
  loc <- O.getAuthenticationRequestUrl (oidc env) [O.openId, O.profile] (Just state) []
  pure $ pack $ show loc

handleLogin :: OIDCEnv -> Handler NoContent
handleLogin env = do
  loc <- genOIDCUrl env
  redirects loc
  pure NoContent

handleToken :: Handler NoContent
handleToken = pure NoContent

authServer :: OIDCEnv -> Server AuthApi
authServer env = handleLogin env :<|> handleToken

server :: OIDCEnv -> Server SuperApi
server env = authServer env :<|> emptyServer :<|> emptyServer

app :: OIDCEnv -> Application
app env = serve (Proxy :: Proxy SuperApi) $ server env

main :: IO ()
main = do
  putStrLn "~~ Super ~~"
  oidcEnv <- initOidc

  run 8081 $ app oidcEnv
