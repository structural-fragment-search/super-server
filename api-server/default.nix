let
  pkgs = import <nixpkgs> { };

  haskellPackages = pkgs.haskellPackages.override {
    overrides = hself: hsuper: {
      "api-server" =
        hself.callCabal2nix "api-server" ./. { };
    };
  };

  shell = haskellPackages.shellFor {
    packages = p: [
      p."api-server"
    ];
    buildInputs = [
      pkgs.haskell-language-server
      pkgs.cabal-install
      pkgs.stylish-haskell
      pkgs.hlint
    ];
    withHoogle = true;
  };
in
{
  inherit shell;
  inherit haskellPackages;
  "api-server" = haskellPackages."api-server";
}
