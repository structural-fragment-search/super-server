# Software Design

## Architecture

```

                                                                           /----------------------------\
                                                                     |---->| Query and process PDB data |
Internet                API Server(s)                                |     \----------------------------/
========                =============                                |
                      |                                              |     /------------------------\   /---------\
/-----------------\   |  /--------------\         /----------\       |---->| Super Database wrapper |-->| superdb |
|   Web Browser   |   |  | API Server   |-------->| Postgres |       |     \------------------------/   \---------/
|                 |   |  | - checks PDB |         \----------/       |
| Web UI (client) |   |  | - auth       |         /------------\     |     /---------------\            /-------\
\-----------------/   |  | - track jobs |-------->| Beanstalkd |---------->| Super Wrapper |----------->| Super |
                      |  \--------------/         \------------/           \---------------/            \-------/
```

### Web Client
- Runs in a web-browser
- Optional auth (OpenID Connect; at least for admin e.g. manual database update; require auth to store results)
- Display WebGL results

#### Technology options

1. TypeScript + React + Redux
1. PureScript + React Basic Hooks
1. PureScript + Halogen
1. Elm

### API Server
- Implements OpenID Connect for login
- Queries the PDB for e.g. PDBID validation, query coordinates fetching (should possibly be offloaded?)
- Job submission and tracking

#### Technology options
1. Python + FastAPI + SQLAlchemy + aiostalk
1. Haskell + Servant + Rel8 + Hbeanstalk
1. PureScript + HTTPure (or Payload) + wrap(node-beanstalk) + sequelize
1. Rust + Rocket + Diesel + beanstalkc-rust

### Notes
* Using PureScript on front-end and server should allow sharing some code
* Haskell/Servant can generate Purescript (`servant-purescript`) or Elm (`servant-elm`) accessors
